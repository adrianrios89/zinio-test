import express from 'express';        // call express
import Router from './routes/Router';
const port =  8080;

const app = express();

app.listen(port);
Router.initListeners(app);
