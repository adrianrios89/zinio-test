import articles from '../data/articles.json'
import { appendDomainToImages} from '../utils/utils';

class Articles {
  appendDomainToImages(htmlToParse){
    return true;
  }

  findById(req, res) {
    const id = Number(req.params.id);

    const article = articles.data.find((article)=>{
      return article.id === id;
    });

    article.body = appendDomainToImages(article.body);

    if(article){
      return res.send({
        body: article.body,
        title: article.title,
        authors: article.authors,
        id: article.id
      });
    }
    res.send(404, 'No articles found');
  }

  findByText(req, res) {
    const searchParam = req.query.q;
    const articleFiltered = articles.data
    .map((article)=>{
      return {
        body: article.body,
        title: article.title,
        authors: article.authors,
        id: article.id
      }
    })
    .filter((article)=>{
      const articleWithRestrictedProps = JSON.stringify(article);
      return searchParam !== '' && articleWithRestrictedProps.includes(searchParam);
    });

    if(articleFiltered){
      return res.send(articleFiltered);
    }
  }

  getAllIndex(req, res) {
    const articlesIndex = articles.data.map((article)=>{
      return article.id
    });

    return res.send(articlesIndex);
  }
}

export default new Articles();
