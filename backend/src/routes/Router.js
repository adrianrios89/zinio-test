import ArticlesController from '../controllers/Articles'

class Router {
  initListeners(app){
      // Allow Cross Domain
      app.use((req, res, next) => {
        // Website you wish to allow to connect
        res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8081');

        // Request methods you wish to allow
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

        // Request headers you wish to allow
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

        // Set to true if you need the website to include cookies in the requests sent
        // to the API (e.g. in case you use sessions)
        res.setHeader('Access-Control-Allow-Credentials', true);

        // Pass to next layer of middleware
        next();
    });
    app.get('/articles/', ArticlesController.getAllIndex);
    app.get('/articles/:id', ArticlesController.findById);
    app.get('/articles/:q?', ArticlesController.findByText);
  }
}

export default new Router();
