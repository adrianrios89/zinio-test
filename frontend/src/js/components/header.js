import React, { Component } from 'react';

export default class Header extends Component {
  render() {
    return (
      <div>
        <nav className="navbar navbar navbar-inverse">
          <div className="container">
            <div className="navbar-header">
              <a className="navbar-brand" href="/article-detail">Articles</a>
              <a className="navbar-brand" href="/search">Search</a>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}
