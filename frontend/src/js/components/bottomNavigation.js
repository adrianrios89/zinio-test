import React from 'react';
import PropTypes from 'prop-types';

const BottomNavigation = ({ isTheFirst, isTheLast, previousArticleId, nextArticleId, getArticlesById }) => (
  <div className="article-navigation">
    <button type="button" className="btn btn-primary" style={{ display: isTheFirst ? 'none' : 'block' }} onClick={() => { getArticlesById(previousArticleId); }} > {'<<'} </button>
    <button type="button" className="btn btn-primary" style={{ display: isTheLast ? 'none' : 'block' }} onClick={() => { getArticlesById(nextArticleId); }} > {'>>'} </button>
  </div>
  );

BottomNavigation.propTypes = {
  previousArticleId: PropTypes.number.isRequired,
  nextArticleId: PropTypes.number.isRequired,
  isTheFirst: PropTypes.bool.isRequired,
  isTheLast: PropTypes.bool.isRequired,
  getArticlesById: PropTypes.func.isRequired
};

export default BottomNavigation;
