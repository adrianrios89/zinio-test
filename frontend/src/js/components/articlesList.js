import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ArticlesList extends Component {
  renderArticles() {
    const articlesListComponent = [];
    this.props.articlesBySearch.forEach((article) => {
      articlesListComponent.push(
        <li className="list-group-item" key={article.id}>{article.title}</li>
      );
    });
    return articlesListComponent;
  }

  render() {
    return (
      <ul className="list-group">
        {this.renderArticles()}
      </ul>
    );
  }
}

ArticlesList.propTypes = {
  articlesBySearch: PropTypes.array.isRequired
};

export default ArticlesList;
