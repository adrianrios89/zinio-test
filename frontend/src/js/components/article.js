import React from 'react';
import PropTypes from 'prop-types';

const Article = ({ articleData }) => (
  <div className="panel panel-default" >
    <div className="panel-heading">
      <h3 className="panel-title">{articleData.title}</h3>
    </div>
    <div className="panel-body article-body">
      <h3>Author: {articleData.authors && articleData.authors[0]}</h3>
      <div dangerouslySetInnerHTML={{ __html: articleData.body }} />
    </div>
  </div>
  );

Article.propTypes = {
  articleData: PropTypes.object
};

export default Article;
