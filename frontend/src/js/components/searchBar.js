import React, { Component } from 'react';
import PropTypes from 'prop-types';

class SearchBar extends Component {
  componentWillMount() {
    this.debounce = null;
  }

  onInputChange(value) {
    this.debounce && window.clearTimeout(this.debounce);
    this.debounce = setTimeout(() => { this.props.getArticlesBySearch(value); }, 300);
  }

  render() {
    return (
      <div className="row">
        <div className="col-md-12">
          <div className="input-group">
            <span className="input-group-addon">
              <i className="fa fa-search" aria-hidden="true" />
            </span>
            <input type="text" className="form-control" autoFocus="true" onChange={event => this.onInputChange(event.target.value)}/>
          </div>
        </div>
      </div>
    );
  }
}

SearchBar.propTypes = {
  getArticlesBySearch: PropTypes.func.isRequired
};

export default SearchBar;
