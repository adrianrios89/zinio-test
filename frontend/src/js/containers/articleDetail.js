import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getArticlesById, fetchArticlesList } from '../actions/articles';

import Article from '../components/article';
import BottomNavigation from '../components/bottomNavigation';

class ArticleDetail extends Component {
  componentWillMount() {
    this.props.fetchArticlesList();
  }

  render() {
    if (this.props.currentArticle) {
      return (
        <div className="container">
          <Article articleData={this.props.currentArticle} />
          <BottomNavigation
            isTheFirst={this.props.isTheFirst}
            isTheLast={this.props.isTheLast}
            previousArticleId={this.props.previousArticleId}
            nextArticleId={this.props.nextArticleId}
            getArticlesById={this.props.getArticlesById}
          />
        </div>
      );
    }
    return null;
  }
}


const mapStateToProps = (state) => {
  return ({
    currentArticle: state.articles.currentArticle,
    previousArticleId: state.articles.previousArticleId,
    nextArticleId: state.articles.nextArticleId,
    isTheFirst: state.articles.isTheFirst,
    isTheLast: state.articles.isTheLast,
  });
};

const mapDispatchToProps = (dispatch) => {
  return ({
    fetchArticlesList: () => {
      dispatch(fetchArticlesList());
    },
    getArticlesById: (id) => {
      dispatch(getArticlesById(id));
    },
  });
};

ArticleDetail.propTypes = {
  currentArticle: PropTypes.object,
  previousArticleId: PropTypes.number.isRequired,
  nextArticleId: PropTypes.number.isRequired,
  isTheFirst: PropTypes.bool.isRequired,
  isTheLast: PropTypes.bool.isRequired,
  fetchArticlesList: PropTypes.func.isRequired,
  getArticlesById: PropTypes.func.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(ArticleDetail);
