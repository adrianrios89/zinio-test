import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getArticlesBySearch } from '../actions/articles';

import SearchBar from '../components/searchBar';
import ArticlesList from '../components/articlesList';

class Search extends Component {
  render() {
    return (
      <section>
        <div className="container">
          <SearchBar getArticlesBySearch={this.props.getArticlesBySearch} />
          <ArticlesList articlesBySearch={this.props.articlesBySearch} />
        </div>
      </section>
    );
  }
}


const mapStateToProps = (state) => {
  return ({
    articlesBySearch: state.articles.articlesBySearch,
  });
};

const mapDispatchToProps = (dispatch) => {
  return ({
    getArticlesBySearch: (searchParam) => {
      dispatch(getArticlesBySearch(searchParam));
    }
  });
};

Search.propTypes = {
  articlesBySearch: PropTypes.array.isRequired,
  getArticlesBySearch: PropTypes.func.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);
