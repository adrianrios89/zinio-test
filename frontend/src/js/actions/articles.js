import { DOMAIN } from '../constants/apiUrls';
import { ON_ARTICLES_LIST_RECEIVED, ON_ARTICLES_BY_SEARCH_RECEIVED, ON_ARTICLE_BY_ID_RECEIVED } from '../constants/actionTypes';

export const onArticlesListReceived = (data) => {
  return {
    type: ON_ARTICLES_LIST_RECEIVED,
    data
  };
};

export const onArticlesBySearchReceived = (data) => {
  return {
    type: ON_ARTICLES_BY_SEARCH_RECEIVED,
    data
  };
};

export const onArticleByIdReceived = (data) => {
  return {
    type: ON_ARTICLE_BY_ID_RECEIVED,
    data
  };
};

export const getArticlesBySearch = (searchParam) => {
  return (dispatch) => {
    return fetch(`${DOMAIN}/articles?q=${searchParam}`).then((response) => {
      return response.json().then((data) => {
        dispatch(onArticlesBySearchReceived(data));
      });
    });
  };
};

export const getArticlesById = (id) => {
  return (dispatch) => {
    return fetch(`${DOMAIN}/articles/${id}`).then((response) => {
      return response.json().then((data) => {
        dispatch(onArticleByIdReceived(data));
      });
    });
  };
};

export const fetchArticlesList = () => {
  return (dispatch) => {
    return fetch(`${DOMAIN}/articles/`).then((response) => {
      return response.json().then((data) => {
        const firstArticleId = data[0];
        dispatch(onArticlesListReceived(data));
        dispatch(getArticlesById(firstArticleId));
      });
    });
  };
};
