import { ON_ARTICLES_LIST_RECEIVED, ON_ARTICLES_BY_SEARCH_RECEIVED, ON_ARTICLE_BY_ID_RECEIVED } from '../constants/actionTypes';

const initialState = {
  currentArticle: null,
  previousArticleId: -1,
  nextArticleId: -1,
  isTheFirst: false,
  isTheLast: false,
  articlesIds: [],
  articlesBySearch: []
};

const events = (state = initialState, action) => {
  switch (action.type) {
    case ON_ARTICLES_LIST_RECEIVED:
      return Object.assign({}, state, {
        articlesIds: action.data
      });
    case ON_ARTICLES_BY_SEARCH_RECEIVED:
      return Object.assign({}, state, {
        articlesBySearch: action.data
      });
    case ON_ARTICLE_BY_ID_RECEIVED:
      const currentArticle = action.data;
      const currentArticleIndex = state.articlesIds.indexOf(currentArticle.id);
      const isTheFirst = currentArticleIndex === 0;
      const isTheLast = currentArticleIndex === state.articlesIds.length - 1;
      const previousArticleId = !isTheFirst ? state.articlesIds[currentArticleIndex - 1] : -1;
      const nextArticleId = !isTheLast ? state.articlesIds[currentArticleIndex + 1] : -1;


      return Object.assign({}, state, {
        currentArticle,
        previousArticleId,
        nextArticleId,
        isTheFirst,
        isTheLast
      });
    default:
      return state;
  }
};

export default events;
