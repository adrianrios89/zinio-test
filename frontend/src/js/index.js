import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { createBrowserHistory } from 'history';
import { Route } from 'react-router-dom';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';
import reducers from './reducers';
import Header from './components/header';
import Search from './containers/search';
import ArticleDetail from './containers/articleDetail';

// browser history
const history = createBrowserHistory();

// middleware
const middleware = routerMiddleware(history);

// create store
const store = createStore(reducers,
    applyMiddleware(middleware, thunk)
);

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history} >
      <div>
        <Route path="/" component={Header} />
        <Route exact={true} path="/" component={ArticleDetail} />
        <Route exact={true} path="/search" component={Search} />
        <Route exact={true} path="/article-detail" component={ArticleDetail} />
      </div>
    </ConnectedRouter>
  </Provider>,
    document.getElementById('root')
);
