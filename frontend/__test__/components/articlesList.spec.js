import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import articlesMockup from '../mockups/articles.json';
import ArticlesList from '../../src/js/components/articlesList';


describe('<ArticlesList /> Component', () => {
  it('ArticlesList component should match with the snapshot', () => {
    const tree = renderer.create(
      <ArticlesList articlesBySearch={articlesMockup} />
    );
    const json = tree.toJSON();
    expect(json).toMatchSnapshot();
  });

  const articleShallow = shallow(<ArticlesList articlesBySearch={articlesMockup} />);

  it('ArticlesList should exists', () => {
    expect(articleShallow.length).toEqual(1);
  });

  it('ArticlesList should have one list-group ul', () => {
    expect(articleShallow.find('ul.list-group')).toHaveLength(1);
  });

  it('ArticlesList should have one list-group-item li', () => {
    expect(articleShallow.find('li.list-group-item')).toHaveLength(1);
  });
});
