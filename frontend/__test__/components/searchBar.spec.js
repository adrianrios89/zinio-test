import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import SeachBar from '../../src/js/components/searchBar';


describe('<SeachBar /> Component', () => {
  it('SeachBar component should match with the snapshot', () => {
    const tree = renderer.create(
      <SeachBar getArticlesBySearch={() => {}} />
    );
    const json = tree.toJSON();
    expect(json).toMatchSnapshot();
  });

  const articleShallow = shallow(<SeachBar getArticlesBySearch={() => {}} />);

  it('SeachBar should exists', () => {
    expect(articleShallow.length).toEqual(1);
  });

  it('SeachBar should have one row div', () => {
    expect(articleShallow.find('div.row')).toHaveLength(1);
  });

  it('SeachBar should have one input-group-addon span', () => {
    expect(articleShallow.find('span.input-group-addon')).toHaveLength(1);
  });

  it('SeachBar should have one form-control input', () => {
    expect(articleShallow.find('input.form-control')).toHaveLength(1);
  });
});
