import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import articleMockup from '../mockups/article.json';
import Article from '../../src/js/components/article';


describe('<Article /> Component', () => {
  it('Article component should match with the snapshot', () => {
    const tree = renderer.create(
      <Article articleData={articleMockup} />
    );
    const json = tree.toJSON();
    expect(json).toMatchSnapshot();
  });

  const articleShallow = shallow(<Article articleData={articleMockup} />);

  it('Article should exists', () => {
    expect(articleShallow.length).toEqual(1);
  });

  it('Article should have one panel div', () => {
    expect(articleShallow.find('div.panel')).toHaveLength(1);
  });

  it('Article should have one panel-heading div', () => {
    expect(articleShallow.find('div.panel-heading')).toHaveLength(1);
  });

  it('Article should have one panel-title h3', () => {
    expect(articleShallow.find('h3.panel-title')).toHaveLength(1);
  });

  it('Article should have one panel-body div', () => {
    expect(articleShallow.find('div.panel-body')).toHaveLength(1);
  });
});
