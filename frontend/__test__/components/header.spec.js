import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import Header from '../../src/js/components/header';


describe('<Header /> Component', () => {
  it('Header component should match with the snapshot', () => {
    const tree = renderer.create(
      <Header />
    );
    const json = tree.toJSON();
    expect(json).toMatchSnapshot();
  });

  const articleShallow = shallow(<Header />);

  it('Header should exists', () => {
    expect(articleShallow.length).toEqual(1);
  });

  it('Header should have one navbar nav', () => {
    expect(articleShallow.find('nav.navbar')).toHaveLength(1);
  });

  it('Header should have one container div', () => {
    expect(articleShallow.find('div.container')).toHaveLength(1);
  });
  it('Header should have two navbar-brand a', () => {
    expect(articleShallow.find('a.navbar-brand')).toHaveLength(2);
  });
});
