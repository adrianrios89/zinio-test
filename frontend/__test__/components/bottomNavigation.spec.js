import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import BottomNavigation from '../../src/js/components/bottomNavigation';


describe('<BottomNavigation /> Component', () => {
  it('BottomNavigation component should match with the snapshot', () => {
    const tree = renderer.create(
      <BottomNavigation
        isTheFirst={false}
        isTheLast={false}
        previousArticleId={1}
        nextArticleId={3}
        getArticlesById={() => {}}
      />
    );
    const json = tree.toJSON();
    expect(json).toMatchSnapshot();
  });

  const articleShallow = shallow(<BottomNavigation
    isTheFirst={false}
    isTheLast={false}
    previousArticleId={1}
    nextArticleId={3}
    getArticlesById={() => {}}
  />);

  it('BottomNavigation should exists', () => {
    expect(articleShallow.length).toEqual(1);
  });

  it('BottomNavigation should have one article-navigation div', () => {
    expect(articleShallow.find('div.article-navigation')).toHaveLength(1);
  });

  it('BottomNavigation should have two buttons', () => {
    expect(articleShallow.find('button')).toHaveLength(2);
  });
});
