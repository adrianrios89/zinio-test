import articles from '../../src/js/reducers/articles';
import articleMockup from '../mockups/article.json';
import { ON_ARTICLES_LIST_RECEIVED, ON_ARTICLES_BY_SEARCH_RECEIVED, ON_ARTICLE_BY_ID_RECEIVED } from '../../src/js/constants/actionTypes';

const initialState = {
  currentArticle: null,
  previousArticleId: -1,
  nextArticleId: -1,
  isTheFirst: false,
  isTheLast: false,
  articlesIds: [],
  articlesBySearch: []
};

const initialStateWitArticlesIds = Object.assign({}, initialState, {
  articlesIds: [122, 123, 124]
});

describe('Articles reducer', () => {
  it('should return default state', () => {
    expect(articles(undefined, {})).toEqual(initialState);
  });

  it('Should add articles list ids when ON_ARTICLES_LIST_RECEIVED fired', () => {
    expect(articles(initialState, { type: ON_ARTICLES_LIST_RECEIVED, data: [1, 2, 3, 4] })).toEqual(Object.assign({}, initialState, {
      articlesIds: [1, 2, 3, 4]
    }));
  });

  it('Should add articles list by search when ON_ARTICLES_BY_SEARCH_RECEIVED fired', () => {
    expect(articles(initialState, { type: ON_ARTICLES_BY_SEARCH_RECEIVED, data: [articleMockup] })).toEqual(Object.assign({}, initialState, {
      articlesBySearch: [articleMockup]
    }));
  });

  it('Should set currentArticle and update indexes when ON_ARTICLE_BY_ID_RECEIVED fired', () => {
    expect(articles(initialStateWitArticlesIds, { type: ON_ARTICLE_BY_ID_RECEIVED, data: articleMockup })).toEqual(Object.assign({}, initialStateWitArticlesIds, {
      currentArticle: articleMockup,
      isTheFirst: false,
      isTheLast: false,
      previousArticleId: 122,
      nextArticleId: 124
    }));
  });
});
