import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';
import iso from 'isomorphic-fetch';
import { DOMAIN } from '../../src/js/constants/apiUrls';
import { ON_ARTICLE_BY_ID_RECEIVED, ON_ARTICLES_BY_SEARCH_RECEIVED, ON_ARTICLES_LIST_RECEIVED } from '../../src/js/constants/actionTypes';

import articleMockup from '../mockups/article.json';
import articlesMockup from '../mockups/articles.json';

import { getArticlesBySearch, getArticlesById, onArticlesListReceived } from '../../src/js/actions/articles';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Articles actions test', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  it('should dispatch [ON_ARTICLES_BY_SEARCH_RECEIVED]  when getArticlesBySearch fired', () => {
    nock(DOMAIN)
    .get('/articles?q=ROWLING')
    .reply(200, articlesMockup);

    const expectedActions = [
      { type: ON_ARTICLES_BY_SEARCH_RECEIVED, data: articlesMockup }
    ];

    const store = mockStore();
    return store.dispatch(getArticlesBySearch('ROWLING')).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('should dispatch [ON_ARTICLE_BY_ID_RECEIVED]  when getArticlesById fired', () => {
    nock(DOMAIN)
    .get('/articles/123')
    .reply(200, articleMockup);

    const expectedActions = [
      { type: ON_ARTICLE_BY_ID_RECEIVED, data: articleMockup }
    ];

    const store = mockStore();
    return store.dispatch(getArticlesById(123)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it('Should dispatch ON_ARTICLES_LIST_RECEIVED when onArticlesListReceived fired', () => {
    const expectedActions = [
      { type: ON_ARTICLES_LIST_RECEIVED, data: [1, 2, 3] }
    ];
    const store = mockStore();
    store.dispatch(onArticlesListReceived([1, 2, 3]));
    expect(store.getActions()).toEqual(expectedActions);
  });
});
