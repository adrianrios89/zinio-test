# Zinio Fullstack Test
A React + Redux (in the front) and NodeJS (in the back) web application to show articles information.
## Setup and install

Inside both backend and frontend folders run:
```
npm install
```
## Launch Application 

### Launch backend (port 8080)

Inside backend folder run:
```
npm run dev
```
### Launch frontend (port 8081)
Inside frontend folder run:
```
npm run start
```
To run the test:

```
npm run test
```

## Description
### Backend
#### Brew Description
The backend is built with **Node JS** and we used the framework **Express**(http://expressjs.com/es/) to build the articles' API and its endpoints. The code is written with **ES6** thanks to **Babel** and we have used **nodemon** to have a livereload, very useful in the development.
#### Architecture
The architecture behind the backend is a **MVC**, that will make the app scalable and will help us to add new features in the future easily. The folders distribution is:

* **controllers**: Folder to store the controllers of the application. A **controller** is a singleton class with the responsability to get data, process this data and sent it to the clients through the **REST API**.

* **data**: This is the space reserved to the app data. At this moment there are only an articles.json but in the future could contain the models of a bd for example.

* **routers**: Inside this folder we have a **router** that has the responsability to listen the entering requests and call the **controller** to process it.

* **utils**: Inside this folder we will have different utilities to use through the application.

### Frontend
#### Brew Description
The frontend is built with **React** and **Redux**. Code is written with **ES6**, validated with [**Eslint**](http://eslint.org/) using [**Airbnb configuration**](https://www.npmjs.com/package/eslint-config-airbnb), and tested with [**Jest**](https://facebook.github.io/jest/). We used [**Webpack**](https://webpack.js.org/) as module bundler and [**Sass**](http://sass-lang.com/) as a css preprocessor.
#### Architecture
We choose [**Redux**](http://redux.js.org/docs/basics/Reducers.html) to build the frontend application, the architecture behind this library is [**Flux**](http://facebook.github.io/flux/). 
The main trait of this architecture is that the data flows in one direction. The main parts of **Redux** (and we follow in this application) are:

* **Actions**
* **Containers & Components**
* **Reducers**
* **Store & State**

We used [**thunk**](https://github.com/gaearon/redux-thunk) library to make async actions and [**react-router-redux**](https://github.com/reactjs/react-router-redux) to manage the routing of the application.

## Next Steps & Technical debt

* Make Test and add Eslint in Backend Part
* Finish Frontend Test
* Improve the search function in backend (we should skip html tags or special chars)
* We could improve the app linking the articles rows in search results with article details view
* Add documentation
* In a real app, we should split backend and frontend in two projects